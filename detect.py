#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import os
import sys
import OPi.GPIO as GPIO
import orangepi.pi4
import threading
from socket import *

host = "127.0.0.1"
port = 6666
ADDR = (host,port)
buffsize = 2048

BOARD = orangepi.pi4.BOARD
GPIO.setmode(BOARD)
GPIO.setup(11, GPIO.IN, GPIO.PUD_DOWN)
GPIO.setup(12, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(18, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(15, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(16, GPIO.OUT, initial=GPIO.LOW)
global golang_client
global probe



def reset():
    os.system("rm projectconfig/authority.txt")
    os.system("cp projectconfig/reset.txt projectconfig/authority.txt")
#探测重置键被长时间按下，进行系统重置，重启
def GpioProbe():
    print("here\n")
    while True:
        GPIO.wait_for_edge(11,GPIO.RISING,timeout=60000)
        time.sleep(3)
        if(GPIO.input(11) == GPIO.HIGH):
            print("fuwei\n")
            reset()
            GPIO.output(12,GPIO.LOW)
            GPIO.output(18,GPIO.LOW)
            GPIO.output(15,GPIO.LOW)
            GPIO.output(16,GPIO.LOW)
            os.system("echo orangepi | sudo -S reboot" )

#当前的系统的状态展示
def system_status():
    global golang_client
    global probe
    while True:
        data = golang_client.recv(buffsize).decode()
        data = str(data)
        if data == "":
            print("data empty")
        else:
            datas = data.split(":")
            if datas[0] == "push":
                if datas[1] == "ok":
                    GPIO.output(15,GPIO.LOW)
                    GPIO.output(16,GPIO.HIGH)
                else:
                    GPIO.output(15,GPIO.HIGH)
                    GPIO.output(16,GPIO.LOW)
            else:
                if datas[1] == "ok":
                    GPIO.output(12,GPIO.LOW)
                    GPIO.output(18,GPIO.HIGH)
                else:
                    GPIO.output(12,GPIO.HIGH)
                    GPIO.output(18,GPIO.LOW)

#网络
GPIO.output(12,GPIO.HIGH)
GPIO.output(18,GPIO.LOW)
#推流
GPIO.output(15,GPIO.HIGH)
GPIO.output(16,GPIO.LOW)

global probe
probe = threading.Thread(target=GpioProbe, name='LoopProbe')
probe.start()

#等待golang客户端接入
golang_client = socket(AF_INET,SOCK_DGRAM)
golang_client.bind(ADDR)

change_status = threading.Thread(target=system_status, name='LoopStatus')
change_status.start()





