package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"time"
	"unicode/utf8"

	"github.com/gin-gonic/gin"
)

// todo：把init初始化写成并行的协程，其中参数初始化必须在最前面，打开热点可以并行
// todo：开机判断当前
func init() {
	// todo把参数初始化写成一个函数
	/***********初始化全局变量****************/
	log.Println("***************start init******************")
	apssid = filecontent("./projectconfig/authority.txt", "apssid", "", 0)
	appassword = filecontent("./projectconfig/authority.txt", "appassword", "", 0)
	curRes = filecontent("./projectconfig/authority.txt", "curRes", "", 0)
	curResCode, _ = strconv.Atoi(filecontent("./projectconfig/authority.txt", "curResCode", "", 0)) //读取curResCode
	roterssid = filecontent("./projectconfig/authority.txt", "roterssid", "", 0)
	wlanmac = filecontent("./projectconfig/authority.txt", "wlanmac", "", 0) //wlan0的mac地址不会变化，可以直接使用文件里面有的，wlp1s0的mac地址会变化，需要每次开机读取一遍
	// log.Println("wlanmac = ", wlanmac)

	//如果apssid为空，则判断机器为首次开机，所以设置apssid为"ShishiWifi_"+wlan0的mac地址，防止热点ssid重复；密码默认12345678
	if apssid == "" {
		apssid = APssidPrefix + wlanmac
		appassword = "12345678"
		log.Println("apssid = ", apssid)
		log.Println("appassword = ", appassword)
		filecontent("./projectconfig/authority.txt", "apssid", apssid, 1)         //将ssid写入文件，下次判断就不是首次开机了
		filecontent("./projectconfig/authority.txt", "appassword", appassword, 1) //将初始密码写入文件
	}

	// 获取网卡名称、网卡MAC地址，网络连接，设置热点和路由器连接
	getAndsetNetInfo()

	// // 获取两张网卡的mac地址，存起来备用
	// if wlanmac == "" {
	// 	log.Println("wlanmac is empty")
	// 	wlanmac = getMac("wlan0") //wlan0是开启ap的网卡
	// }
	// wlp1smac = getMac("wlp1s0") //wlp1s0是连接路由器的网卡

	// ActivateAp()

	log.Println("***************end init******************")
}

func main() {
	// deleteAp()
	//stoped()
	startStreamer()
	go probe()
	time.Sleep(2 * time.Second) //休眠2秒，等待数据流推送起来，然后拉流播放
	go Play_Video()
	r := gin.Default()
	r.GET("/:query", deal)
	// r.POST("/on_play_auth", postDeal)
	r.Run() // listen and serve on 0.0.0.0:8080
}

//处理http请求
//TODO: 需要返回给请求端一定的数据，待整理
func deal(c *gin.Context) {
	query := c.Param("query")
	// log.Println(query)
	if query == "getroterlist" {
		c.String(200, getroterlist())
	} else if query == "setroter" {
		ssid := c.DefaultQuery("ssid", "")
		password := c.DefaultQuery("password", "")
		if ssidlen := utf8.RuneCountInString(ssid); ssidlen > 20 || ssidlen < 1 { // 0<ssid长度<21
			c.String(200, errorReturn(paramerror, "ssid must be [1-20] characters"))
		} else if passwlen := utf8.RuneCountInString(password); passwlen < 8 || passwlen > 20 { // 7<passw长度<21
			c.String(200, errorReturn(passwerror, "password must be [8-20] characters"))
		} else {
			resp := setroter(ssid, password)
			c.String(200, resp)
		}
	} else if query == "getvideocoding" {
		c.String(200, getvideocoding())
	} else if query == "setResolution" {
		log.Println("setResolution")
		resCodeStr := c.DefaultQuery("resCode", "0")
		resCode, _ := strconv.Atoi(resCodeStr) //转成数字
		c.String(200, setResolution(resCode))
	} else if query == "update" {
		url := c.DefaultQuery("url", "")
		c.String(200, update(url))
	} else if query == "setauthority" {
		password := c.DefaultQuery("password", "")
		c.String(200, setauthority(password))
	} else if query == "setap" {
		ssid := c.DefaultQuery("ssid", "")
		password := c.DefaultQuery("password", "")
		if ssidlen := utf8.RuneCountInString(ssid); ssidlen > 20 || ssidlen < 1 { // 0<ssid长度<21
			c.String(200, errorReturn(paramerror, "ssid must be [1-20] characters"))
		} else if passwlen := utf8.RuneCountInString(password); passwlen < 8 || passwlen > 20 { // 7<passw长度<21
			c.String(200, errorReturn(passwerror, "password must be [8-20] characters"))
		} else {
			c.String(200, succReturn(retok, "set ap success", nil))
			go setap(ssid, password)
		}
		// log.Println(ssid, "succReturn(retok,\"set ap ok\", nil)")
	} else if query == "ipModel" {
		ifname := c.DefaultQuery("ifname", "wlan")
		method := c.DefaultQuery("model", "dhcp")
		address := c.DefaultQuery("IPAddress", "")
		subnetmask := c.DefaultQuery("SubnetMask", "")
		gateway := c.DefaultQuery("DefaultGateway", "")
		dns1 := c.DefaultQuery("DNSServer1", "")
		dns2 := c.DefaultQuery("DNSServer2", "")
		log.Println(ifname, method, address, subnetmask, gateway, dns1, dns2)
		c.String(200, ipModel(ifname, method, address, subnetmask, gateway, dns1, dns2))
	} else if query == "getsysinfo" {
		c.String(200, getsysinfo())
	} else if query == "getwaninfo" {
		c.String(200, getwaninfo())
	} else if query == "changescreen" {
		screen := c.DefaultQuery("screen", "")
		c.String(200, changescreen(screen))

	}
}

//用来处理视频拉流鉴权的请求
// ffplay -fflags nobuffer rtmp:192.168.31.221/hls/video?password=123456   在视频拉取时写入
// func postDeal(c *gin.Context) {
// 	password := c.PostForm("password")
// 	//首先要去读取文件中设置的密码是多少，然后进行比较
// 	oriPassword := filecontent("./projectconfig/authority.txt", "password", "", 0)
// 	log.Println(oriPassword)
// 	if password == oriPassword {
// 		// log.Println("jianquanlaile\r\n")
// 		c.String(200, "post")
// 	} else {
// 		log.Println("password error\r\n")
// 		c.String(404, "post")
// 	}
// }

//用来设置推流鉴权的密码
//http://192.168.31.221:8080/setauthority?password=123456
func setauthority(password string) string {
	//打开文件，将密码修改进去
	return filecontent("./projectconfig/authority.txt", "password", password, 1)
}

// http://192.168.31.240:8080/update?url=http://47.95.10.123:8080/bin_file/main
func update(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		return err.Error()
	}
	defer resp.Body.Close()

	//对url切片，获取文件名
	// urlslice := strings.Split(url, "/")
	// filename := urlslice[len(urlslice)-1]
	// 创建一个文件用于保存
	out, err := os.Create(Dir + "/" + "tmp")
	if err != nil {
		return err.Error()
	}
	defer out.Close()

	// 然后将响应流和文件流对接起来
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err.Error()
	}

	//覆盖原二进制文件
	command := `echo "orangepi" | sudo -S mv tmp main`
	cmd := exec.Command("/bin/bash", "-c", command)
	// log.Println(cmd)
	err = cmd.Run() //阻塞
	if err != nil {
		return "Execute Command failed:" + err.Error()
	}

	command = `echo "orangepi" | sudo -S chmod 775 main`
	cmd = exec.Command("/bin/bash", "-c", command)
	// log.Println(cmd)
	err = cmd.Run() //阻塞
	if err != nil {
		return "Execute Command failed:" + err.Error()
	}
	return "Download OK"
}

// //关闭easydarwin,找到easydarwin的pid，然后杀掉
// func stoped() {
// 	log.Println("stop start")
// 	command := `ps -e | grep "easydarwin" | awk '{print $1}'`
// 	cmd := exec.Command("/bin/bash", "-c", command)
// 	bytes, err := cmd.Output()
// 	if err != nil {
// 		log.Println(err)
// 	} else {
// 		pid := string(bytes)
// 		pids := strings.Split(pid, "\n")
// 		log.Println(pids)
// 		if len(pids)==3 {
// 			syscall.Kill(-EdPush.Process.Pid, syscall.SIGKILL)
// 		}else{
// 			log.Println("easydarwin pid empty")
// 		}
// 	}
// 	log.Println("stop ed ok")
// }
