# 毕业设计开发记录

## 0. 设备选型

* OrangePi4主板+电源+PCIE转接板+散热片

* 外置网卡+转接板


## 1. 开发环境搭建

### 1.1 手册下载

OrangePi资源下载，里面有如何使用orangepi，如何在orangepi上烧录ubuntu，以及相关的设置：

http://www.orangepi.cn/downloadresourcescn/OrangePi4/2019-12-16/orangepi4_usermanual1.html

### 1.2 OrangePi设备

* 解决中文字符支持问题：

```
sudo locale-gen en_US.UTF-8
```

将 `export LC_ALL=en_US.UTF-8` 加入到 `/etc/profile` 末尾，然后
```
source /etc/profile
```


* 如果使用桌面会比较卡，需要使用 ssh 连接 orangePi，建议使用 *mobaxterm* 连接 orangePi，可以直接使用 sftp，比较方便
* 可以设置一下路由器，给 orangePi 设置一个静态IP，这样不需要每次开机都得查看一遍IP。

### 1.3 推流环境搭建

使用 RTSP 协议进行推流

#### 查看摄像头参数

```shell
sudo v4l2-ctl --list-formats-ext -d /dev/video10
```

#### 推流操作

#### 1. 下载   推流软件：

项目地址：[https://github.com/EasyDarwin/EasyDarwin](https://github.com/EasyDarwin/EasyDarwin)

编译方式：[https://blog.csdn.net/weixin_43866471/article/details/108301078](https://blog.csdn.net/weixin_43866471/article/details/108301078)

编译好的项目：[https://wwa.lanzous.com/izH1Tk7hpdc](https://wwa.lanzous.com/izH1Tk7hpdc)

**使用方式**：

直接下载编译好的项目直接使用即可 

* 直接运行

  ```shell
  cd easyDarwin
  ./easydarwin
  # Ctrl + C
  ```

* 以服务启动

  ```shell
  cd EasyDarwin
  ./start.sh
  # ./stop.sh
  ```

* 查看界面

  打开浏览器输入 [http://10.42.0.1:10008](http://localhost:10008/), 进入控制页面,默认用户名密码是 admin/admin

#### 2. 使用 GStreamer 将摄像头采集视频推到FIFO文件中：

首先新建一个 FIFO 文件：

```shell
mkfifo ~/rtsp.264
```

利用 gstreamer 将编码好的视频流推到 fifo 文件中：

```shell
gst-launch-1.0 v4l2src device=/dev/video10 ! "image/jpeg,width=1280,height=720,framerate=30/1" ! jpegdec ! videoconvert ! queue ! mpph264enc ! filesink location = ~/test.264
```

#### 3. 使用 FFmpeg 将 FIFO 文件中的视频流推送到 easyDarwin 服务器：

```shell
ffmpeg -re -i ~/test.264 -codec copy -f rtsp -rtsp_transport tcp rtsp://10.42.0.1:8554
```

#### 4. 拉流操作

* 手机拉流：

  保证手机跟 orangepi 在一个局域网中，手机下载 EasyPlayerRTSP（由EasyDarwin团队开发和维护的RTSP流媒体播放器，RTSP版本的支持Windows(支持多窗口、包含ActiveX，npAPI Web插件)、Android平台，iOS平台，视频支持H.264，H.265，MPEG4，MJPEG，音频支持G711A，G711U，G726，AAC，支持RTSP over TCP/UDP，支持硬解码，是一套极佳的RTSP流播放组件，安卓下载：http://app.tsingsee.com/EasyRTSPlayer，IOS 去 AppStore 中搜索下载。

  输入 `rtsp://192.168.1.8:8554` 即可查看摄像头画面。

* 电脑拉流：

  电脑用ffplay查看视频流（linux下指令，windows自行查找命令）
  
  ```
  ffplay -fflags nobuffer rtsp://10.42.0.1:8554
  ```

## 2. Golang

### 2.1 版本

go1.14.13 ：
https://studygolang.com/dl

### 2.2 教程

安装Go：
```bash
#使用wget下载go的二进制文件
wget https://dl.google.com/go/go1.10.3.linux-arm64.tar.gz
#使用tar将源码包提取到/usr/local目录中
sudo tar -C /usr/local -xzf go1.10.3.linux-amd64.tar.gz
#调整环境变量
sudo vim /etc/profile
# 在最后一行添加
export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin
export GOPROXY=https://goproxy.cn
export GO111MODULE=on
# 保存退出后source一下（vim 的使用方法可以自己搜索一下）
source /etc/profile
#测试是否安装成功
go version
```

这是一个很好的golang的网络编程的例子，跟着手敲一遍可以很好的理解：
https://www.cnblogs.com/aaronthon/p/10951929.html

go mod的使用（先了解gopath）

https://www.jianshu.com/p/760c97ff644c

Gin框架教程：

https://geektutu.com/post/quick-go-gin.html

## 3. 网络环境配置

### 3.1 两个网卡工作流程

之前考虑的是大网卡做为内网网卡，内置小网卡连接上层路由器。但是后来实验发现大网卡无法开启802.11ac的网络，只能开启133Mbps带宽的802.11a网络，带宽太小，无法承载多台设备同时拉流。

由此考虑换一个顺序，用外置的大网卡连接上层路由器，用内置的小网卡开启热热点作为视频推流的内网，内置的小网卡可以开启802.11ac协议的WiFi热点，速度可以达到433Mbps。

## 4. 开机自启动特定可执行程序
### 4.1 systemd设置启动服务配置文件
在/etc/systemd/system目录下添加shishi.service文件，添加配置：
```
[Unit]
Description=shishi_Service
After=display-manager.service

[Service]
Type=simple
Restart=always
RestartSec=5s
ExecStart=/home/orangepi/shishi/start.sh
WorkingDirectory=/home/orangepi/shishi/

[Install]
WantedBy=multi-user.target
```
[Unit]中After字段是选择在哪个服务启动之后再启动该服务，选择在桌面启动之后启动改程序，否则在启动时桌面界面无法正常显示。

ExecStart是一个sh脚本，脚本去启动在shishi目录下的main可执行程序
WorkingDirectory指定程序运行时的目录，不指定找不到shishi目录下的authority.txt配置文件。
### 4.2 service文件开机启动生效
此命令用于使能service开机时启动
```
sudo systemctl enable shishi.service
```
此命令用于立即启动服务

```
sudo systemctl start shishi.service
```
此命令用于查看服务的状态
```
sudo systemctl status shishi.service
```

**注意一个问题：从windows拷贝到orangepi的可执行文件必须使用chmod +x修改一下文件类型**


# 附录手册

## 1. orangepi设置

+ 安装easyDarwin
+ 安装FFmpeg
+ 安装golang
+ 设置gomod，goproxy
    + export GO111MODULE=on
    + export GOPROXY=https://goproxy.cn

+ 安装v4l2-ctl `sudo apt install v4l2-ctl`

+ 安装OPI-GPIO
```
sudo apt install python3-pip
pip3 install setuptools
git clone https://github.com/baiywt/OPi.GPIO.git
cd OPi.GPIO
sudo python3 setup.py install
```

## 2. http配置协议
+ 获取可连接的上层路由SSID以及各种信息
```
http://10.42.0.1:8080/getroterlist
```

+ 连接上层路由
```
http://10.42.0.1:8080/setroter?ssid=要连接的SSID&password=密码
```

+ 设置盒子热点
```
http://10.42.0.1:8080/setap?ssid=要设置的SSID&password=要设置的password
```

+ ~~设置盒子热点SSID~~(deprecated)

  ~~`http://10.42.0.1:8080/setapssid?ssid=要设置的SSID`~~


+ ~~设置盒子热点的密码~~(deprecated)

  ~~`http://10.42.0.1:8080/setappassword?password=要修改的密码`~~


+ 获取当前摄像头的参数（当前只有分辨率）
```
http://10.42.0.1:8080/getvideocoding
```

+ 设置当前推流的摄像头的分辨率
```
http://10.42.0.1:8080/setResolution?resolution=宽度x高度
```

+ 返回桌面
```
http://10.42.0.1:8080/changescreen?screen=Desktop
```
+ 获取系统信息
```
http://10.42.0.1:8080/getsysinfo
```

+ 获取系统信息
```
http://10.42.0.1:8080/getwaninfo
```


## 3.拉流播放
+ 将手机，或者电脑连接到盒子创建的热点
+ 使用EasyPlayer APP,拉流地址为：
```
rtsp://10.42.0.1:8554
```

