package main

import (
	"bufio"
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

// 获取网卡信息，包括名字，连接名字，网卡MAC地址，并配置网络
// nmcli -e no -g GENERAL.DEVICE,GENERAL.CONNECTION,GENERAL.HWADDR device show
func getAndsetNetInfo() string {
	log.Println("getNetInfo")
	command := `echo "orangepi"|sudo -S nmcli -e no -g GENERAL.DEVICE,GENERAL.CONNECTION,GENERAL.HWADDR device show`
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: getAndsetNetInfo\n")
	bytes, err := cmd.Output() //返回值中包含LF换行符，需要去掉
	if err != nil {
		//log.Println("getNetInfo cmd.Run", err)
		shishilog("getNetInfo cmd.Run error: " + err.Error() + "\n")
		return "" //todo 如何处理这个错误
	}

	tmp := string(bytes)
	// log.Println(string(bytes))
	slice := strings.Split(tmp, "\n")
	var wlancon string
	var wlp1scon string

	// 每四个一组寻找网卡、连接和MAC地址
	for i := 0; i < len(slice); i++ {
		// log.Println(slice[i])
		// 寻找网卡名wlan和wlp1s
		if slice[i][:1] == "w" {
			if slice[i][:4] == WLAN {
				WLAN = slice[i] //0 wlan0
				log.Println("wlan = ", slice[i])
				i++
				wlancon = slice[i] //1
				// if wlancon == "" {
				// 	log.Println("wlancon is empty")
				// }
				// log.Println("wlancon = ", slice[i])
				i++
				wlanmac = slice[i] //2 保存mac地址
				log.Println("wlanmac = ", slice[i])
				i++ //3

			} else if slice[i][:4] == WLP1S {
				WLP1S = slice[i]
				log.Println("WLP1S = ", slice[i])
				i++
				wlp1scon = slice[i] //1
				// if wlp1scon == "" {
				// 	log.Println("wlp1scon is empty")
				// }
				// log.Println("wlp1scon = ", slice[i])
				i++
				wlp1smac = slice[i] //2 保存mac地址
				log.Println("wlp1smac = ", slice[i])
				i++ //3
			}
		} else {
			i += 3
		}
	}

	//将mac地址写入文件
	filecontent("./projectconfig/authority.txt", "wlanmac", wlanmac, 1)
	shishilog("Write " + wlanmac + " wlanmac " + "to authority.txt\n")
	filecontent("./projectconfig/authority.txt", "wlp1smac", wlp1smac, 1)
	shishilog("Write " + wlp1smac + " wlp1smac " + "to authority.txt\n")

	//判断有无连接，此处不显示有可能是因为连接没有激活，不代表连接不存在
	if wlancon == "" { //热点未开启，需判断热点连接是否存在，存在则up，不存在则setap
		log.Println("wlan no connection")
		if getConn(apssid) { //连接存在，启用连接
			upConn(apssid, WLAN)
		} else {
			log.Println("热点不存在，创建")
			setap(wlanmac, appassword) //setap中有添加前缀的操作
		}
	}
	if wlp1scon == "" { //路由器未连接，如果roterssid为空，则跳过不连；如果不为空，打开连接，打开失败拉倒
		log.Println("wlp1s no connection")
		if roterssid != "" { //roterssid不为空才开启
			if !upConn(roterssid, WLP1S) { //开机打开路由器连接，若无连接我也没有办法，因为我没有密码
				deleteRoter()
			}
		}
	}
	return tmp
}

// 获取网卡mac地址，并写入文件
// nmcli -e no -g GENERAL.HWADDR device show wlp1s0
// func getMac(ifname string) string {
// 	log.Println("get" + " " + ifname + " " + "Mac")
// 	command := `echo "orangepi"|sudo -S nmcli -e no -g GENERAL.HWADDR device show` + " " + ifname
// 	cmd := exec.Command("/bin/bash", "-c", command)
// 	bytes, err := cmd.Output() //返回值中包含LF换行符，需要去掉
// 	if err != nil {
// 		log.Println("getMac cmd.Run", err)
// 		return ""
// 	}
// 	retv := string(bytes[:17]) //切片防止cmd返回信息里面的回车信息
// 	filecontent("./projectconfig/authority.txt", ifname+"mac", retv, 1)
// 	return retv
// }

// 获取当前所有的连接，判断连接是否存在
// nmcli -e no -g NAME connection show显示所有连接
func getConn(conn string) bool {
	log.Println("get connection")

	// 先判断当前热点是否开启
	command := `echo "orangepi"|sudo -S nmcli -e no -g NAME connection show`
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: getConn\n")
	bytes, err := cmd.Output() //返回值中包含LF换行符，需要去掉
	if err != nil {
		//log.Println("getConn cmd.Run", err)
		shishilog("getConn cmd.Run error: " + err.Error() + "\n")
		return false
	}
	tmp := string(bytes)
	// log.Println(string(bytes))
	slice := strings.Split(tmp, "\n")

	// 寻找热点是否开启，如果开启则返回
	for i := 0; i < len(slice); i++ {
		log.Println(slice[i])
		if slice[i] == conn {
			log.Println(conn, "连接存在")
			return true
		}
	}
	log.Println("连接不存在")
	return false
}

// 获取路由器信息
// http://10.42.0.1:8080/getroterlist
//// nmcli -m tabular -t -f SSID,CHAN,RATE,SIGNAL,SECURITY d wifi list ifname wlp1s0 | awk -F: '{for(i=1;i<=NF;++i) print $i}'
// nmcli -e no -g SSID,CHAN,RATE,SIGNAL,SECURITY,BSSID d wifi list ifname wlp1s0 | awk -F: '{for(i=1;i<=NF-6;++i) print $i}{print $6":"$7":"$8":"$9":"$10":"$11}'
func getroterlist() string {
	rescanRoter()
	command := `nmcli -e no -g SSID,CHAN,RATE,SIGNAL,SECURITY,BSSID d wifi list ifname` + " " + WLP1S + " " + `| awk -F: '{for(i=1;i<=NF-6;++i) print $i}{print $6":"$7":"$8":"$9":"$10":"$11}'`
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: getroterlist \n")
	bytes, err := cmd.StdoutPipe()
	if err != nil { //创建管道失败
		//log.Println("getaplist cmd.StdoutPipe err:", err)
		shishilog("getaplist cmd.StdoutPipe err: " + err.Error() + "\n")
		return errorReturn(internalerror, "getaplist cmd.StdoutPipe err")
	}
	err = cmd.Start()
	if err != nil { //cmd start失败
		//log.Println("getaplist cmd.Start err:", err)
		shishilog("getaplist cmd.Start err: " + err.Error() + "\n")
		return errorReturn(internalerror, "getaplist cmd.Start err")
	}

	reader := bufio.NewReader(bytes)
	allAPjson := []aplist{} //结构体数组，用于生成json
	var APjson aplist       //结构体用于保存每一个wifi信息
	//实时循环读取输出流中的一行内容
	var line []byte
	var ismyself bool
	for {
		ismyself = false
		//读取ssid
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF1")
			break
		}
		ssid := string(line)
		APjson.SSID = ssid
		if ssid == apssid {
			ismyself = true
			// log.Println("ssid == apssid")
		}

		//读取wifi信道
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF2")
			break
		}
		channel := string(line)
		APjson.CHAN = channel

		//wifi速率
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF3")
			break
		}
		rate := string(line)
		APjson.RATE = rate

		//wifi信号强度
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF4")
			break
		}
		signal := string(line)
		APjson.SIGNAL = signal

		//wifi认证方式
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF5")
			break
		}
		security := string(line)
		APjson.SECURITY = security

		//wifi BSSID即mac地址
		//根据ssid和bssid（mac地址）判断是不是自己开的热点
		line, _, err = reader.ReadLine()
		if err != nil || io.EOF == err {
			log.Println("reader.ReadLine EOF6")
			break
		}
		bssid := string(line)
		APjson.BSSID = bssid
		if ismyself {
			// log.Println("bssid %d", []byte(bssid))
			// log.Println("wlanmac %d", []byte(wlanmac))
			if bssid == wlanmac {
				// log.Println("bssid == wlanmac")
				continue //跳过自己开的ap
			}
		}

		allAPjson = append(allAPjson, APjson) //添加一条wifi信息，循环添加直到完成
	}

	jsonStr := succReturn(retok, "success", allAPjson) //生成json
	// log.Println(string(json))
	err = cmd.Wait()
	return jsonStr
}

// 设置连接路由器，首先删除之前的连接，在设置新的连接
// sudo nmcli dev wifi connect iPhone password xch123456 ifname  wlp1s0
// http://10.42.0.1:8080/setroter?ssid=iPhone&password=xch123456
func setroter(ssid string, password string) string {
	deleteRoter()
	rescanRoter()
	//log.Println(ssid, password)
	filecontent("./projectconfig/authority.txt", "roterssid", ssid, 1) //将修改写进配置文件

	roterssid = ssid
	shishilog("Write roterssid " + ssid + " to authority.txt\n")
	// filecontent("./projectconfig/authority.txt", "wifipassword", password, 1)
	command := `echo "orangepi" | sudo -S nmcli device wifi connect`
	command = command + " " + ssid + " " + "password" + " " + password + " " + "ifname" + " " + WLP1S

	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: setroter\n")
	bytes, err := cmd.CombinedOutput()

	// log.Printf("setroter bytes:%c\n", tmp)
	if err != nil { //没有ssid情况
		//log.Println("setroter err:", err)
		shishilog("setroter cmd.Combined error:" + err.Error() + "\n")
	}

	tmp := string(bytes)
	//log.Println("setroter bytes:", tmp)
	shishilog("Setroter bytes: " + tmp + "\n")
	// 字符串切片判断是否含有Error或者successfully，从而构造相应的返回值
	slice := strings.Split(tmp, " ")

	var resp string
	if slice[0] == "Error:" {
		if slice[1] == "No" {
			//没有指定ssid的wifi，返回信息是No network with SSID 'ssid参数' found.
			resp = errorReturn(paramerror, "无此WiFi，请查找后重新输入")
			shishilog("Error: No this WiFi\n")
			filecontent("./projectconfig/authority.txt", "roterssid", "", 1) //删除ssid，因为没有ssid的时候系统不会创建连接，需要把文件中的内容删除
			shishilog("Delete roterssid\n")
		} else if slice[1] == "Connection" {
			//密码错误，返回信息是Connection activation failed: (7) Secrets were required, but not provided.
			resp = errorReturn(passwerror, "WiFi密码错误，请重新输入")
			shishilog("Error: WiFi password error\n")
			deleteRoter() //密码错误的情况下 orangepi也会创建连接，所以需要把文件内容和创建失败的连接删除

		}
	} else if slice[2] == "successfully" {
		//连接成功，返回信息是Device 'wlp1s0' successfully activated with '72c75692-4217-45a7-ad74-0243f6d0f60b'.
		resp = succReturn(retok, "连接成功", nil)
		shishilog("Connection successful\n")
	}

	return resp
}

// 该函数会添加一个 "ShishiWifi_" 前缀，注意！！！
// 设置热点，需要做边界处理，密码最少8位，最多20位；热点ssid最少1位，最多20位
// http://10.42.0.1:8080/setap?ssid=xchwifi&password=12345678
// sudo nmcli device wifi hotspot ifname wlan0 con-name xchwifi ssid xchwifi band a channel 149 password 12345678
func setap(ssid string, password string) bool {
	deleteAp()                                                        //删除当前ssid的热点
	apssid = APssidPrefix + ssid                                      //修改apssid全局变量
	appassword = password                                             //修改appassword全局变量
	filecontent("./projectconfig/authority.txt", "apssid", apssid, 1) //将修改存入文件
	shishilog("Write " + apssid + " apssid " + "to authority.txt\n")
	filecontent("./projectconfig/authority.txt", "appassword", password, 1)

	shishilog("Write " + password + " appassword " + "to authority.txt\n")
	command := `echo "orangepi" | sudo -S nmcli device wifi hotspot ifname` + " " + WLAN + " " + `con-name` + " " + apssid + " " + "ssid" + " " + apssid + " " + "band a channel 149 password" + " " + appassword
	// log.Println(command)
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: setap\n")
	err := cmd.Run()
	if err != nil {
		//log.Println("setap err:", err)
		shishilog("Setap command error:" + err.Error() + "\n")
		return false
	}
	return true
}

// 若有连接，使用nmcli打开连接，使连接可用，返回true；如果没有连接则，创建失败，返回false
//nmcli connection up shishi ifname wlan0
func upConn(ssid string, ifname string) bool {
	//log.Println(ssid, "upConn")
	shishilog(ssid + " upConn \n")
	command := `echo "orangepi"|sudo -S nmcli connection up` + " " + ssid + " " + "ifname" + " " + ifname
	// log.Println("command = ", command)
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: upConn\n")
	err := cmd.Run()
	if err != nil {
		//log.Println(ssid, "no conn:", err.Error())
		shishilog("no conn error:" + err.Error() + "\n")
		return false //表示无连接，需创建链接 * 这个不能表示没有wifi，只能表示返回错误，需要改*
	}
	return true //表示有链接，直接使能连接
}

// 若有连接，使用nmcli打开连接，使连接可用，返回true；如果没有连接则，创建失败，返回false
//nmcli connection modify ShishiWifi_xx ifname wlan0 connection.autoconnect yes
func setAutoCon(connect string, ifname string) bool {
	shishilog(connect + " setAutoCon \n")
	command := `echo "orangepi"|sudo -S nmcli connection modify` + " " + connect + " " + "ifname" + " " + ifname + " " + `connection.autoconnect yes`
	log.Println("command = ", command)
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: setAutoCon\n")
	err := cmd.Run()
	if err != nil {
		return false
	}
	return true //表示有链接，直接使能连接
}

//重新扫描wifi，防止wifi列表只有一个已连接的wifi
func rescanRoter() {
	log.Println("rescanRoter")
	command := `echo "orangepi"|sudo -S nmcli d wifi rescan ifname` + " " + WLP1S
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: rescanRoter\n")
	err := cmd.Run()
	if err != nil {
		//log.Println("rescanRoter cmd.Run", err)
		shishilog("rescanRoter cmd.Run error：" + err.Error() + "\n")
	}
	time.Sleep(3 * time.Second)
}

//删除路由连接，防止重复创建
func deleteRoter() {
	log.Println("deleteRoter")
	command := `echo "orangepi" | sudo -S nmcli con delete` + " " + roterssid
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: deleteRoter\n")
	err := cmd.Run()
	if err != nil {
		//log.Println("deleteRoter cmd.Run err:", err)
		shishilog("deleteRoter cmd.Run error：" + err.Error() + "\n")
	}
	filecontent("./projectconfig/authority.txt", "roterssid", "", 1) //删除ssid
	shishilog("Delete roterssid\n")
}

//删除热点，防止重复创建
func deleteAp() {
	// apssid := filecontent("./projectconfig/authority.txt", "apssid", "", 0)
	command := `echo "orangepi" | sudo -S nmcli con delete` + " " + apssid
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: deleteAp\n")
	err := cmd.Run()
	if err != nil {
		//log.Println("deleteAp err:", err)
		shishilog("deleteAp cmd.Run error：" + err.Error() + "\n")
	}
}

//设置静态ip时，使用nmcli con modify命令发现，如果当前使用自动ip，则设置静态ip没有问题；若当前为静态ip，则设置动态ip后会出现两个ip，两个ip都能用。但是两个ip在设置静态ip之后又会变为一个ip
//http://10.42.0.1:8080/ipModel?ifname=lan&model=staticIp&IPAddress=192.168.80.124&SubnetMask=255.255.255.0&DefaultGateway=192.168.80.2&DNSServer1=8.8.8.8&&DNSServer2=192.168.1.1
//http://10.42.0.1:8080/ipModel?ifname=wlan&model=dhcp
func ipModel(ifname string, model string, address string, subnetmask string, gateway string, dns1 string, dns2 string) string {
	if ifname == "wlan" {
		ifname = WLP1S
	}
	if ifname == "lan" {
		ifname = "eth0"
	}
	// 获取设备的连接名
	command := `echo "orangepi" | sudo -S nmcli -g GENERAL.CONNECTION device show` + " " + ifname
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: Device show\n")
	bytes, err := cmd.Output()
	if err != nil {
		//log.Println("setnet cm.StdoutPipe err:", er)
		shishilog("setnet cm.StdoutPipe err: " + err.Error() + "\n")
	}
	// log.Println(bytes)
	connection := string(bytes[:len(bytes)-1]) //去掉最后的回车
	// log.Println("connection =", connection)

	// 设置连接
	if model == "dhcp" { //dhcp模式
		command = `echo "orangepi" | sudo -S nmcli connection modify` + " " + connection + " " + "ifname" + " " + WLP1S + " " + `ipv4.method auto connection.autoconnect yes`
	} else if model == "staticIp" { //静态ip
		//nmcli connection modify Xiaomi_6B4C ifname wlp1s0 ipv4.method manual ipv4.addresses 192.168.31.110/24 ipv4.gateway 192.168.31.1 ipv4.dns "192.168.31.1 8.8.8.8" connection.autoconnect yes
		if dns2 == "" {
			command = `echo "orangepi" | sudo -S nmcli connection modify` + " " + connection + " " + "ifname" + " " + WLP1S + " " + `ipv4.method manual ipv4.addresses` + " " + address + "/" + transMask(subnetmask) + " " + "ipv4.gateway" + " " + gateway + " " + "ipv4.dns \"" + dns1 + "\" connection.autoconnect yes"
		} else {
			command = `echo "orangepi" | sudo -S nmcli connection modify` + " " + connection + " " + "ifname" + " " + WLP1S + " " + `ipv4.method manual ipv4.addresses` + " " + address + "/" + transMask(subnetmask) + " " + "ipv4.gateway" + " " + gateway + " " + "ipv4.dns \"" + dns1 + " " + dns2 + "\" connection.autoconnect yes"
		}
	} else {
		command = ""
	}

	// log.Println("command =", command)
	cmd = exec.Command("/bin/bash", "-c", command)
	shishilog("Command: setnet \n")
	err = cmd.Run()
	if err != nil {
		//log.Println(err)
		shishilog("setnet cmd.run err: " + err.Error() + "\n")
		return err.Error()
	}
	//开启连接
	upConn(connection, ifname)
	return succReturn(retok, "set ok", nil)
}

//trans 255.255.255.0 to 24
func transMask(subnetmask string) string {
	arr := strings.Split(subnetmask, ".")
	cnt := 0
	if len(arr) < 4 {
		return "0"
	}
	for i := 0; i < 4; i++ {
		b, _ := strconv.Atoi(arr[i])
		for ; b != 0; b >>= 1 {
			if b&1 != 0 {
				cnt = cnt + 1

			}
		}
	}
	return strconv.Itoa(cnt)
}

// 判断当前设备是否连接上了，以及网络信息：状态、网关、ip地址和掩码、dnsserver、mac
// nmcli -e no -g GENERAL.STATE,IP4.GATEWAY,IP4.ADDRESS,IP4.DNS,GENERAL.HWADDR device show wlp1s0
func getDevcieStatus(device string) lineList {
	log.Println("getDevcieStatus")
	var line lineList = lineList{}
	// 先判断当前设备连接状态
	command := `echo "orangepi"|sudo -S nmcli -e no -g GENERAL.STATE,IP4.GATEWAY,IP4.ADDRESS,IP4.DNS,GENERAL.HWADDR device show` + " " + device
	cmd := exec.Command("/bin/bash", "-c", command)
	bytes, err := cmd.Output() //返回值中包含LF换行符，需要去掉
	if err != nil {
		log.Println("getConn cmd.Run", err)
		return line
	}
	tmp := string(bytes)
	// log.Println(tmp)
	slice := strings.Split(tmp, "\n")
	// log.Println(slice)
	// mac地址
	line.MAC = slice[len(slice)-2]

	if len(slice) == 3 { //无连接
		line.STATUS = "down"
	} else {
		if slice[len(slice)-6] == "100 (connected)" { //有连接
			line.STATUS = "up"
			line.DNSSERVER = slice[len(slice)-3]
			line.IPMASK = slice[len(slice)-4][len(slice[len(slice)-4])-2:]
			line.IPADDR = slice[len(slice)-4][:len(slice[len(slice)-4])-3]
			line.GATEWAY = slice[len(slice)-5]
		} else { //无连接
			line.STATUS = "down"
		}

	}
	return line
}

// 用于获得系统信息
// http://10.42.0.1:8080/getsysinfo
func getsysinfo() string {
	var info sysinfo   //结构体用于保存信息
	info.SSID = apssid //盒子的热点ssid
	// nmcli -g 802-11-wireless.channel connection show apssid
	// todo：以后添加可变channel，现在全返回149
	info.CHANNEL = "149"
	info.ROTERSSID = roterssid

	// 有连接就是有网，先判断wlp1s0
	if getDevcieStatus(WLP1S).STATUS == "up" { //有路由器连接
		info.LINK = roterssid
		info.ENABLE = "1"
		info.WORKLINE = "wlan"
	} else {
		info.LINK = ""
		info.ENABLE = "0"
		info.WORKLINE = ""
	}
	if getDevcieStatus("eth0").STATUS == "up" { //有线网有连接
		info.ENABLE = "1"
		info.WORKLINE = "lan"

	}

	info.MODULE = module
	info.FIRMWAREVER = firmwareVer
	info.FIRMWARENAME = firmwareName

	jsonStr := succReturn(retok, "", info)

	log.Println(jsonStr)
	return jsonStr
}

// 用于获得网络信息
// http://10.42.0.1:8080/getwaninfo
func getwaninfo() string {
	var info netInfo
	var linelist []lineList
	var wlanline lineList
	var lanline lineList

	wlanline = getDevcieStatus(WLP1S)
	wlanline.LANNAME = "wlan"
	lanline = getDevcieStatus("eth0")
	lanline.LANNAME = "lan"
	// 有连接就是有网，先判断wlp1s0
	if wlanline.STATUS == "up" { //有路由器连接
		info.WORKLINE = "wlan"
	} else {
		info.WORKLINE = ""
	}

	if lanline.STATUS == "up" { //有线网有连接
		info.WORKLINE = "lan"
	}
	log.Println(wlanline)
	log.Println(lanline)

	linelist = append(linelist, wlanline)
	linelist = append(linelist, lanline)
	info.LINELIST = linelist
	return succReturn(retok, "", info)

}

//nmcli device show wlan0 | grep IP4.ADDRESS  | awk '{if(NF>=2) {print 1 }else {print 0}}'
func ifdevice(device string) string {
	var line []byte
	command := `echo "orangepi" | sudo -S nmcli device show ` + device + ` | grep IP4.ADDRESS  | awk '{if(NF>=2) {print 1 }else {print 0}}'`
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: ifdevice\n")
	bytes, err := cmd.StdoutPipe()
	if err != nil {
		//log.Println(err)
		shishilog("ifdevice cmd.StdoutPipe err: " + err.Error() + "\n")
	}
	cmd.Start()
	reader := bufio.NewReader(bytes)
	line, _, err = reader.ReadLine()
	if err != nil || io.EOF == err {
		log.Println("EOF1")
	}
	return string(line)
}

func ifnet() string {
	var line []byte
	command := "ping www.baidu.com -c 1 | grep min | awk '{if(NF>=1) {print 1} else {print 0}}'"
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: ifnet\n")
	bytes, err := cmd.StdoutPipe()
	if err != nil {
		//log.Println(err)
		shishilog("ifnet cmd.StdoutPipe err: " + err.Error() + "\n")
	}
	cmd.Start()
	reader := bufio.NewReader(bytes)
	line, _, err = reader.ReadLine()
	if err != nil || io.EOF == err {
		log.Println("EOF1")
	}
	log.Println(string(line))
	return string(line)
}

//   nmcli device | grep wlp1s0 | awk '{if("--"==$4) {print 0 }else {print 1}}'
