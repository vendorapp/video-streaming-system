package main

import (
	"io/ioutil"
	"log"
	"fmt"
	"net"
	"os/exec"
	"strings"
	"syscall"
	
)

//Dir 程序执行目录，与升级文件存放位置有关
const Dir string = "."

/***********全局变量****************/
//文件中的全局变量
var curRes string  //分辨率
var curResCode int //当前分辨率代码
var apssid string
var appassword string
var roterssid string //连接的路由器的ssid
var wlanmac string
var wlp1smac string

//其他全局变量
var WLP1S string = "wlp1" //连接路由器的网卡；存储是wlp1s0还是wlp1s1... ，先初始化
var WLAN string = "wlan"  //开热点的网卡；存储是wlan0还是wlan1... ，先初始化

var APssidPrefix string = "ShishiWifi_" //热点名字的前缀

var resName []string //分辨率名字列表
var resNum int       //分辨率数量

var module string = "SSBOX-1"
var firmwareVer int = 12
var firmwareName string = "3.2.16"

var cmdPush = exec.Command("/bin/bash", "-c", "ls") //只是用来定义一个全局cmd
var cmdFFmpeg = exec.Command("/bin/bash", "-c", "ls") //只是用来定义一个全局cmd,用来FFmpeg推送空白视频
var cmdPlayVideo = exec.Command("/bin/bash", "-c", "ls") //只是用来定义一个全局cmd,用来播放video

/***********全局变量****************/

//http返回json数据
type retJson struct {
	CODE    int         `json:"code"`
	MESSAGE string      `json:"message"`
	DATA    interface{} `json:"data,omitempty"`
}

//返回wifi列表的结构体
type aplist struct {
	SSID     string `json:"ssid"`
	CHAN     string `json:"channel"`
	RATE     string `json:"rate"`
	SIGNAL   string `json:"signal"`
	SECURITY string `json:"security"`
	BSSID    string `json:"bssid"`
}

type videocoding struct {
	CURRESCODE int          `json:"curResCode"`
	CURRES     string       `json:"curRes"`
	SUPPORTRES []supportRes `json:"supportRes"`
}

type supportRes struct {
	RESCODE int    `json:"resCode"`
	RES     string `json:"res"`
}

type sysinfo struct {
	SSID         string `json:"ssid"`
	CHANNEL      string `json:"channel"`
	ROTERSSID    string `json:"roterSsid"`
	LINK         string `json:"link"`
	ENABLE       string `json:"enable"`
	WORKLINE     string `json:"workLine"`
	MODULE       string `json:"module"`
	FIRMWAREVER  int    `json:"firmwareVer"`
	FIRMWARENAME string `json:"firmwareName"`
}

type netInfo struct {
	WORKLINE string     `json:"workLine"`
	LINELIST []lineList `json:"lineList"`
}

type lineList struct {
	LANNAME   string `json:"lanName"`
	STATUS    string `json:"status"`
	GATEWAY   string `json:"gateway"`
	IPADDR    string `json:"ipaddr"`
	IPMASK    string `json:"ipmask"`
	DNSSERVER string `json:"dnsserver"`
	MAC       string `json:"mac"`
}

//封装一个函数，用来查找、替换指定文件的某一行的参数值
//第一个参数是指定的文件的名称;
//第二个参数是需要修改的文件中的行的key;
//第三个参数是是需要修改的value值;
//第四个参数值是用来指定是修改参数还是查询参数，1表示修改，0表示查询
//没有value返回""空值
func filecontent(filename string, linekey string, param string, flag int) string {
	var returnStr string
	input, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println("ioutil.ReadFile err:", err)
	}
	lines := strings.Split(string(input), "\n")
	for i, line := range lines { //筛选每一行
		lineStringArray := strings.Split(line, "#")
		linestringkey := string(lineStringArray[0])
		if linestringkey == linekey { //寻找到对应的行
			// log.Printf(line + "\n")
			if flag == 1 {
				lines[i] = linestringkey + "#" + param
				output := strings.Join(lines, "\n")
				err = ioutil.WriteFile(filename, []byte(output), 0644)
				if err != nil {
					log.Println("ioutil.WriteFile err:", err)
				}
				returnStr = "set ok"
				break
			} else {
				returnStr = lineStringArray[1]
				break
			}
		}
	}
	return returnStr
}
//运行CMD命令，探测指定的程序是否在运行
func cmd_probe(command string) int{
	result := 0
	cmd := exec.Command("/bin/bash", "-c", command)
	bytes, err := cmd.Output()
	if err != nil {
		log.Println(err)
	} else {
		pid := string(bytes)
		//log.Println("pid : " + pid)
		if pid != "" {
			result = 1
		}else{
			result = 0
		}
	}
	return result
}
//向Python服务器发送状态数据
func Send_to_Python(data_send string){
	//+++++++++++++++++++++++++连接Python服务器，Python服务器负责系统当前状态的展示++++++++++++++++++++++++++++
	python_server,err := net.Dial("udp","127.0.0.1:6666")
	if err != nil {
		log.Println(err)
	}
	_,er := python_server.Write([]byte(data_send))
	if er != nil {
		log.Println(er)
	}else{
		log.Println("send ok")
	}
}
//检测网络是否接通
func NetWorkStatus() bool {
	cmd := exec.Command("ping", "baidu.com", "-c", "1", "-W", "5")
	err := cmd.Run()
	if err != nil {
	fmt.Println(err.Error())
	return false
	} else {
	fmt.Println("Net Status , OK")
	}
	return true
}
//屏幕播放视频
func Play_Video() {
	log.Println("video playing\n")
	command := "./display.sh"
	cmdPlayVideo = exec.Command("/bin/bash", "-c", command)
	cmdPlayVideo.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmdPlayVideo.Run()
	log.Println("video play down\n")
}
// http://192.168.31.240:8080/changescreen?screen=Desktop
func changescreen(screen string) string {
	log.Println(screen)
	if screen == "Desktop"{//关闭显示程序，然后切换到tty1然后切换到tty7
		//查看现在是否已经在显示视频
		probe_test := cmd_probe(`ps -e | grep "rkmpp_player"| awk '{print $1}'`)
		if probe_test == 1{//在显示视频，需要关闭视频，然后显示桌面
			log.Println("is video")
			syscall.Kill(-cmdPlayVideo.Process.Pid, syscall.SIGKILL) //关闭播放视频的进程

			//切换tty终端
			cmd := exec.Command("/bin/bash", "-c", `echo "orangepi" | sudo chvt 1`)
			_, err := cmd.Output()
			if err != nil {
				log.Println(err)
			}
			cmd = exec.Command("/bin/bash", "-c", `echo "orangepi" | sudo chvt 7`)
			_, err = cmd.Output()
			if err != nil {
				log.Println(err)
			}
			
			log.Println("show desktop")
		}else{//现在就在显示桌面，不用操作
			log.Println("Desktop is playing")
		}
	}else if screen == "Video"{//启动显示程序
		probe_test := cmd_probe(`ps -e | grep "rkmpp_player"| awk '{print $1}'`)
		if probe_test == 1{//在显示视频，不需要操作
			log.Println("video is playing")
		}else{//现在在显示桌面，启动显示视频任务
			go Play_Video()
		}
	}else{
		return errorReturn(paramerror, "请设置正确的切换界面")
	}
	return succReturn(retok, "success", nil)
}