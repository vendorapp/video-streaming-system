package main

import (
	"log"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"time"
)

func probe() {
	time.Sleep(2 * time.Second) //休眠2秒
	//+++++++++++++++++++++++++确定当前的推流状态++++++++++++++++++++++++++++++++++++++++
	streamer_flag := 0                                           //是否在推流标志
	command := `ps -e | grep "gst-launch-1.0"| awk '{print $1}'` //检测是不是在推流
	retu := cmd_probe(command)
	if retu == 1 { //在推流
		streamer_flag = 1
		Send_to_Python("push:ok")
		log.Println("streamer ing")
	} else { //没在推流
		streamer_flag = 0
		Send_to_Python("push:no")
	}
	time_num := 0
	//++++++++++++++++++++++++++++循环查询当前系统的状态，包括推流状态以及网络状态+++++++++++++++++++++++++++++++++
	for {
		//===========================摄像头推流查询=======================
		command = `ls /dev | grep "video10" | awk '{print $1}' ` //检测是不是有摄像头
		cmd := exec.Command("/bin/bash", "-c", command)
		bytes, err := cmd.Output()
		if err != nil {
			log.Println(err)
		} else { //命令运行正确
			video_prob := string(bytes)
			log.Println(video_prob)
			if video_prob != "" && streamer_flag == 0 { //检测到有摄像头,并且之前是没有推流状态，表示此时可以进行运行推流
				startStreamer() //推流
				streamer_flag = 1
				Send_to_Python("push:ok")
			} else if video_prob != "" && streamer_flag == 1 { //有摄像头，或者现在已经在推流
				streamer_flag = 1
				time.Sleep(3 * time.Second) //休眠3秒
				Send_to_Python("push:ok")
			} else { //没有摄像头
				//查看空白视频是否在推送
				command = `ps -e | grep "ffmpeg"| awk '{print $1}'`
				retu = cmd_probe(command)
				if retu == 1 { //空白视频在推送
					log.Println("empty is pushing")
				} else { //空白视频没在推送
					log.Println("empty should push")
					go FFmpeg_Empty()
				}
				streamer_flag = 0
				time.Sleep(3 * time.Second) //休眠3秒
				Send_to_Python("push:no")
			}
		}
		time.Sleep(1 * time.Second) //休眠1秒
		time_num++
		//=========================网络状态查询=====================================
		if time_num == 2 {
			time_num = 0
			net_status := NetWorkStatus()
			if net_status == true {
				Send_to_Python("net:ok")
			} else {
				Send_to_Python("net:no")
			}
		}
	}
}

//试试sudo -A
//开启ffmpeg和Gstreamer
func startStreamer() {
	//获取摄像头分辨率列表
	getvideocoding()
	//使用分辨率进行推流
	setResolution(curResCode)
}

//resCode = 0 1 2...
//设置推流的分辨率10.42.0.1:8080/setResolution?resCode=0
func setResolution(resCode int) string {
	if resCode < 0 || resCode >= resNum {
		return errorReturn(paramerror, "请输入正确分辨率代码")
		shishilog("Error: Resolution incorrect\n")
	}

	//判断分辨率代码没问题，需要将设置的分辨率写入文件，
	log.Println("resCode:", resCode)
	resolution := resName[resCode] //取出全局变量中的分辨率串
	//log.Println("resolution:", resolution)
	shishilog("Resolution is: " + resolution + "\n")
	resolutionArray := strings.Split(resolution, "x")
	width := string(resolutionArray[0])
	height := string(resolutionArray[1])

	//判断gst是否在运行。在运行得话杀掉
	command := `ps -e | grep "gst-launch-1.0"| awk '{print $1}'`
	retu := cmd_probe(command)
	if retu == 1 { //gstreamer在运行，需要先kill
		log.Println("gstreamer is streaming，need to kill")
		syscall.Kill(-cmdPush.Process.Pid, syscall.SIGKILL)
	} else { //没在运行
		log.Println("gstreamer didn't stream")
	}

	//判断是否有摄像头，如果有再进行推流操作
	command = "gst-launch-1.0 v4l2src device=/dev/video10 ! \"image/jpeg,width=" + width + ",height=" + height + "\" ! jpegdec ! videoconvert ! queue ! mpph264enc ! filesink location=/home/orangepi/test.264"
	shishilog("Command: Using gstreamer push the encoded video stream to FIFO file\n")
	//todo：取消判断，在prob里面判断
	// log.Println(command)
	time.Sleep(2 * time.Second) //休眠2秒
	//log.Println("判断camera是否存在")
	command_probe := `ls /dev | grep "video10"| awk '{print $1}'` //检测是不是有摄像头
	cmd := exec.Command("/bin/bash", "-c", command_probe)
	shishilog("Command: Judge cameras\n")
	bytes, err := cmd.Output()
	if err != nil {
		//log.Println("judge camera err:", err)
		shishilog("Judge cameras command error: " + err.Error() + "\n")
		return errorReturn(internalerror, "judge camera err")
	}
	//命令运行正确
	video_prob := string(bytes)
	if video_prob != "" {
		//log.Println("camera存在")
		shishilog("Program status: Have video\n")
		go pushStream(command) //开启gst推流
		go pushStream_ffmpeg() //开启ffmpeg推流
		curResCode = resCode
		curRes = resolution
		filecontent("./projectconfig/authority.txt", "curRes", curRes, 1)
		shishilog("Write " + curRes + " curRes " + "to authority.txt\n")
		filecontent("./projectconfig/authority.txt", "curResCode", strconv.Itoa(curResCode), 1)
		shishilog("Write " + strconv.Itoa(curResCode) + " curResCode " + "to authority.txt\n")
		return succReturn(retok, "success", nil)
	} else {
		log.Println("camera不存在")
		go FFmpeg_Empty() //推送空白视频
		return errorReturn(internalerror, "请插入UVC摄像头")
	}
}

//开启gstreamer推流
func pushStream(com string) {
	log.Println("start gstreamer")
	cmdPush = exec.Command("/bin/bash", "-c", com)
	cmdPush.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmdPush.Run()
	shishilog("Command: Start GStreamer streaming\n")
	//log.Println("gstreamer closed")
}

//开启ffmpeg推流
func pushStream_ffmpeg() {
	//判断空白视频是否在推送，如果在推送，则先去杀掉这个进程
	command := `ps -e | grep "ffmpeg"| awk '{print $1}'`
	retu := cmd_probe(command)
	if retu == 1 { //在推送空白视频
		log.Println("kill empty ffmpeg")
		syscall.Kill(-cmdFFmpeg.Process.Pid, syscall.SIGKILL)
	} else { //没在推送
		log.Println("ffmpeg is not push empty video")
	}
	//推送摄像头数据
	log.Println("start ffmpeg")
	cmd_push_ffmpeg := exec.Command("/bin/bash", "-c", "ffmpeg -re -i /home/orangepi/test.264 -codec copy -f rtsp -rtsp_transport tcp rtsp://10.42.0.1:8554")
	cmd_push_ffmpeg.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd_push_ffmpeg.Run()
	shishilog("Command: Start ffmpeg streaming\n")
	//log.Println("ffmpeg closed")
}

//ffmpeg推送空白视频
func FFmpeg_Empty() {
	log.Println("empty video push_in\n")
	command := "ffmpeg -re -stream_loop -1 -i /home/orangepi/shishi/empty.h264 -codec copy -f rtsp -rtsp_transport udp rtsp://10.42.0.1:8554"
	cmdFFmpeg = exec.Command("/bin/bash", "-c", command)
	cmdFFmpeg.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmdFFmpeg.Run()
	log.Println("empty video push_out\n")
}

//获取摄像头的参数，现在只返回了分辨率这一个参数
//v4l2-ctl --list-framesizes=MJPG -d /dev/video10 | awk -F " " '{if (NR>1)print $3}'
//http://10.42.0.1:8080/getvideocoding
func getvideocoding() string {
	command := `echo "orangepi" | sudo -S v4l2-ctl --list-framesizes=MJPG -d /dev/video10 | awk -F " " '{if (NR>1)print $3}'`
	cmd := exec.Command("/bin/bash", "-c", command)
	shishilog("Command: Getvideocoding \n")
	bytes, err := cmd.Output()
	if err != nil {
		//log.Println("getvideocoding err:", err)
		shishilog("Getvideocoding command error: " + err.Error() + "\n")
		// todo，处理错误
	}
	tmp := string(bytes)
	// log.Println("getvideocoding tmp:", tmp)

	//如果列表为空，则没插入摄像头，返回错误信息
	if tmp == "" {
		// log.Println("返回为空")
		jsonStr := errorReturn(internalerror, "请插入UVC摄像头")
		shishilog("Error: No camera\n")
		// log.Println(jsonStr)
		return jsonStr
	}

	// 对分辨率列表按照回车切片，注意最后一个回车会产生一个空串
	/*640x480
	 *1920x1080
	 *320x240
	 *800x600
	 *1280x720
	 *1024x576
	 */
	reslist := strings.Split(tmp, "\n")
	resNum = len(reslist) - 1 //因为回车在最后，故最后多了一个空字符串，需去掉
	log.Println("resNum =", resNum)
	var iscurResinList bool = false

	resName = []string{} //清零
	var vdjson videocoding
	// 判断当前默认分辨率是否在摄像头分辨率列表中
	for i := 0; i < resNum; i++ {
		// log.Println(reslist[i])
		resName = append(resName, reslist[i]) //保存到全局变量
		if curRes == reslist[i] {             //这是找到了当前分辨率，顺便记下编号
			// log.Println("curRes in list")
			iscurResinList = true
			if curResCode != i { //支持此分辨率但是编号不对，更新编号和文件
				curResCode = i
				filecontent("./projectconfig/authority.txt", "curResCode", strconv.Itoa(curResCode), 1)
				shishilog("Write " + strconv.Itoa(curResCode) + " curResCode " + "to authority.txt\n")
			}
		}
		//把获取到的分辨率填入返回的json中
		vdjson.SUPPORTRES = append(vdjson.SUPPORTRES, supportRes{RESCODE: i, RES: reslist[i]})
	}

	if iscurResinList == false { //不在列表中将分辨率设置为第0个分辨率，并存入文件
		curRes = reslist[0]
		curResCode = 0
		filecontent("./projectconfig/authority.txt", "curRes", curRes, 1)
		shishilog("Write " + curRes + " curRes " + "to authority.txt\n")
		filecontent("./projectconfig/authority.txt", "curResCode", strconv.Itoa(curResCode), 1)
		shishilog("Write " + strconv.Itoa(curResCode) + " curResCode " + "to authority.txt\n")
	}
	vdjson.CURRES = curRes
	vdjson.CURRESCODE = curResCode

	jsonStr := succReturn(retok, "success", vdjson)
	// log.Println(jsonStr)
	return jsonStr
}
