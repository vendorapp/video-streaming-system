package main

import "encoding/json"

type ErrorCode int
type SuccCode int

const ( //枚举类型，错误码
	internalerror ErrorCode = iota + 30000 //30000
	paramerror                             //30001
	passwerror                             //30002
)

const ( //枚举类型，成功码
	retok SuccCode = iota + 20000 //20000
)

func errorReturn(code ErrorCode, message string) string { //错误的返回 eg:cmd处理错误，
	retjson := retJson{
		CODE:    int(code),
		MESSAGE: message,
	}
	json, _ := json.Marshal(retjson) //生成json
	// fmt.Println(string(json))
	return string(json)
}

func succReturn(code SuccCode, message string, data interface{}) string { //正确的返回
	retjson := retJson{
		CODE:    int(code),
		MESSAGE: message,
		DATA:    data,
	}
	json, _ := json.Marshal(retjson) //生成json
	// fmt.Println(string(json))
	return string(json)
}
