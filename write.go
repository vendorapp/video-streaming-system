package main

import (
	"io"
	"log"
	"os"
	"time"

	//"reflect"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

/**
 *判断文件是否存在 存在返回true 不存在返回false
 */

func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

func shishilog(writeString string) {
	//var writeString = "测试写文件zwj123n\n"
	length1 := len(writeString)
	if length1 <= 0 {
		//fmt.Println("输入错误")
		log.Println("输入错误")
		return
	}
	currentTime := time.Now()
	formatNow := currentTime.Format("2006-01-02 15:04:05")
	Time1 := formatNow + "  :  "
	num1 := strings.Index(formatNow, " ")
	other := formatNow[:num1]
	filename := other + ".log"
	writeString = Time1 + writeString
	var f *os.File
	var err1 error
	//var err2 error
	if checkFileIsExist(filename) {
		f, err1 = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0666)
		//fmt.Println("文件存在")
		// log.Println("文件存在")
	} else {
		f, err1 = os.Create(filename) //创建文件
		//fmt.Println("文件不存在")
		log.Println("文件不存在")
	}
	check(err1)
	_, err1 = io.WriteString(f, writeString)
	check(err1)
	//fmt.Printf("写入%d 个字节\n", n)
	// log.Printf("写入%d 个字节\n", n)
	// log.Println(writeString)
}
